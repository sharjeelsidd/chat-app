const users = []

const addUser = ({id, username, room}) => {
    username = username.trim().toLowerCase()
    room = room.trim().toLowerCase()

    // Validate the user
    if(!username || !room){
        return {
            error: 'Username and room are required!'
        }
    }

    // Check for the existing user
    const existingUser = users.find((user)=>{
        return user.room === room && user.username === username 
    })

    // Store user

    const user = { id, username, room}
    users.push(user)
    return { user }
}

addUser({
    id: 22,
    username: 'Andrew',
    room: 'South Park'
})

console.log(users)